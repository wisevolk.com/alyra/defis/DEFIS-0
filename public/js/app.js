const userInputElem = document.querySelector("input");
const resultElem = document.getElementById("result");
const citationElem = document.getElementById("citation");

const getExpression = (conversionType, userInput, callback) => {
    fetch("", {
        method: "POST",
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ conversionType, userInput })
    })
        .then(res => res.json())
        .then(res => {
                if ( res.error )
                    callback(res.error);
                else
                    callback(undefined, res);
            }
        );
};

const submitConversion = (e => {
    e.preventDefault();
    e.stopPropagation();
    const buttonName = e.target.id;
    resultElem.textContent = "";
    citationElem.textContent = "";
    getExpression(buttonName, userInputElem.value, (error, { errorInput, citation, userInput="", resultat="" } = {}) => {
        //TODO résoudre l'Affichage des erreurs
        citationElem.textContent = "Loading...";
        if ( error || errorInput) {
            // console.log("errorInput.length: ",errorInput.length);
            citationElem.textContent = (error) ? error : errorInput;
        } else {
            resultElem.textContent = `${ userInput }  =====>  ${ String(resultat).toUpperCase() }`;
            citationElem.textContent = citation;
            // console.log("erreurInput : ", errorInput);
        }
        userInputElem.value = "";
    });
});

document.querySelectorAll("button")
    .forEach(elem => elem.addEventListener("click", submitConversion, false));



