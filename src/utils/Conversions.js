const converters = require("./Formulas");

const base16Regex = new RegExp(/[0-9a-f]+[a-f]{1}[^g-z]/i);
const base10Regex = new RegExp(/^[0-9]+/);
const base2Regex = new RegExp(/\d+[^2-9a-z]/i);

const isDecimal = (num) => {
    return base10Regex.test(num) === true;
}

const isHex = (num) => {
    return base16Regex.test(num) === true;
}

const isBinary = (num) => {
    return base2Regex.test(num) === true;
}

const isValidInput = (num) => {
    return isHex(num) || isDecimal(num) || isBinary(num);
}

const checkInput = (conversionType, userInput) => {

    userInput = userInput.trim();
    if ( (userInput || userInput.length > 0) && isValidInput(userInput) ) {
        if ( (conversionType === "toHex") && isDecimal(userInput) ) {    // Decimal ==> Hex
            console.log("// Decimal ==> Hex");
            return {
                decimal: userInput,
                result: converters.decimalToHex(userInput)
            };
        } else if ( (conversionType === "toHex") && isBinary(userInput) ) {   // Bin ==> Hex
            console.log("// Bin ==> Hex");
            return {
                decimal: converters.binToDecimal(userInput),
                result: converters.binToHex(userInput)
            };
        } else if ( (conversionType === "toDecimal") && isHex(userInput) ) {       // Hex ==> Decimal
            console.log("// Hex ==> Decimal");
            return {
                decimal: converters.hexToDecimal(userInput),
                result: converters.hexToDecimal(userInput)
            };
        } else if ( (conversionType === "toDecimal") && isBinary(userInput) ) {        // Bin ==> Decimal
            console.log("// Bin ==> Decimal")
            return {
                decimal: converters.binToDecimal(userInput),
                result: converters.binToDecimal(userInput)
            };
        } else if ( (conversionType == "toBin") && isHex(userInput) ) {         // Hex ==> Bin
            console.log("// Hex ==> Bin")
            return {
                decimal: converters.hexToDecimal(userInput),
                result: converters.hexToBin(userInput)
            };
        } else if ( (conversionType == "toBin") && isDecimal(userInput) ) {      // Decimal ==> Bin
            console.log("// Decimal ==> Bin")
            return {
                decimal: userInput,
                result: converters.decimalToBin(userInput)
            };
        } else {
            if ( isBinary(userInput) || isDecimal(userInput) || isHex(userInput) ) {
                throw new Error("Je ne peux convertir un type en lui-même !!");
            }
        }
    } else {
        throw new Error("Veuillez entrer un Entier ou un Hexadécimal ou un Binaire");
    }
}

module.exports = {
    checkInput
};
