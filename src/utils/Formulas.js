const decimalToHex = (decimal) => {
    const hexNumber = [];
    while ( decimal !== 0 ) {
        const reste = Math.trunc(decimal / 16);
        let hex = decimal - (reste * 16);
        if ( hex > 9 )
            hex = String.fromCharCode(hex + 55);

        //console.log(hex);
        hexNumber.push(hex);
        decimal = Math.trunc(decimal / 16);
    }
    return hexNumber.reverse().join("");
    /*if(dec < 16)
        hexNumber.push(dec);
    let reste;
    while ( dec > 16 ) {
        reste = Math.floor(dec / 16)
        let hex = dec - (reste * 16);
        if ( hex > 9 )
            hex = hexMap.get(hex);
        console.log(hex);
        hexNumber.push(hex);
        dec = decimalToHex(reste);
    }*/
};

const decimalToBin = (decimal) => {
    const binNumber = [];
    while ( decimal !== 0 ) {
        (decimal % 2 == 0) ? binNumber.push(0) : binNumber.push(1);
        decimal = Math.trunc(decimal / 2);
    }
    return binNumber.reverse().join("");
};

const hexToDecimal = (hex) => {
    let i = 0;
    let decimal = 0;
    hex.split("")
        .reverse()
        .forEach(c => {
            if ( c.charCodeAt(0) > 64 ) c = c.charCodeAt(0) - 55;
            decimal += Number(c) * Math.pow(16, i);
            i++;
        });
    return decimal;
};

const hexToBin = (hex) => {
  return decimalToBin(hexToDecimal(hex));
};

const binToDecimal = (bin) => {
    let temp = 0;
    bin.toString().split("")
        .forEach(n => {
            temp = (temp * 2) + Number(n);
        });
    return temp;
};

const binToHex = (bin) => {
    return decimalToHex(binToDecimal(bin));
};

module.exports = {
    decimalToHex,
    decimalToBin,
    hexToDecimal,
    hexToBin,
    binToDecimal,
    binToHex
};

/*const hexNumber = decimalToHex(317547);
console.log(hexNumber);
const decNumber = hexToDecimal("4D86B");
console.log(decNumber);
const binNumber = decimalToBin(156);
console.log(binNumber);
const decNumberFromBin = binToDecimal(1011001);
console.log(decNumberFromBin);
console.log(binToHex(1011001));*/
