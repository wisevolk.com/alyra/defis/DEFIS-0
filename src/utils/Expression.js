const request = require("request");

const expression = (expressionNumber, callback) => {
    const url = `http://numbersapi.com/${ expressionNumber }?json`;
    request(url, (error, { body }) => {
        if ( error ){
            callback(error, undefined);
        }else{
            callback(undefined,JSON.parse(body));
    }});

}

module.exports = expression;
