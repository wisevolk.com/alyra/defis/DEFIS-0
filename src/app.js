const path = require("path");
const express = require("express");
const hbs = require("hbs");
const expression = require("./utils/Expression");
const converters = require("./utils/Conversions");

const app = express();
const port = process.env.PORT || 3000;

const publicDirectory = path.join(__dirname, "../public");
const viewsPath = path.join(__dirname, "../templates/views");
const partialsPath = path.join(__dirname, "../templates/partials");

hbs.registerPartials(partialsPath);

app.set("view engine", "hbs");
app.set("views", viewsPath);

app.use(express.static(publicDirectory));
app.use(express.json());

app.post("/", (req, res) => {
    const { conversionType, userInput } = req.body;
    console.log("userInput : ", userInput === undefined);
    try {
        console.log(req.body);
        const { decimal, result }={} = converters.checkInput(conversionType, userInput) ;
        console.log("Après Converter : ", userInput, result)
        expression(decimal, (error, { text }) => {
            if ( error ) {
                console.log("Error")
                return res.send({ error });
            } else {
                console.log(text, "/", userInput, "/", result);
                return res.send({
                    citation: text,
                    userInput,
                    resultat: result
                });
            }
        });
    } catch (e) {
        console.log(e.message);
        return res.send({ errorInput: e.message });
    }
})

app.get("", (req, res) => {
        res.render("index", {
            title: "Alyra - Défis 0"
        })
    }
);

app.get("*", (req, res) => {
    res.render("404", {
        title: "Erreur 404"
    });
});


app.listen(port, () => {
    console.log(`Server on localhost is listening on port ${ port }`);
});

